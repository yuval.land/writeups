# PicoCTF 2018
- ## Flaskcards (350)
- ## Flaskcards Skeleton Key (600)
- ## Flaskcards and Freedom (900)
##### by Yuval Meshorer

<br>
## Flaskcards - Points: 350
![](flaskcards 1 - intro.png)

Opening the site, we have the option to register a new account.
![http://2018shell1.picoctf.com:17991](Flaskcards 1 - Site.png)
Clicking "Register" opens a new page where we can create a new account. For somplicity we'll use:
```
Username: Yuval
Password: Yuval
```
After creating an account and logging in, we are greeted with a new menu.
![](Flaskcards 1 - Welcome.png)
> *Note: the tab "Admin" is for the next part of the challenge, "Flaskcards Skeleton Key"*

Inferring from the name "Flaskcards" we can (correctly) assume that the challenge involes SSTI (Server Side Template Injection). To those unfamiliar with what it is, *"Template Injection occurs when user input is embedded in a template in an unsafe manner."* (https://portswigger.net/blog/server-side-template-injection)

The only place where we can input data apart from the login page is in "Create Cards".
![](Flaskcards 1 - Create Card.png)

Since we know this challenge has something to do with Template Injection, we can check if the site is vulnerable to injection. We'll type `{{7*7}}` in the "Question" field and `{{7*'7'}}`in the "Answer" field. We'll create the card and then go to "List Cards" to see if the injection worked.

![](Flaskcards 1 - 7x7.png)

We can see that we successfully injected code that was interpreted as a template. `{{7*7}}` has turned into `49` and `{{7*'7'}}` has turned into `7777777`

Now, let's try to find the flag.

Most SSTI tutorials recommend first trying to input `{{self}}` and `{{config}}` which is what we'll do. We'll input `{{config}}` into the "Question" part of "Create Card" and `{{self}}` into the answer. Creating the card and going back to "List Cards", this is what we see:

![](Flaskcards 1 - Config.png)

Although the config looks messy and not clear, looking at the 2nd to last line, we see the flag!
`'SECRET_KEY': 'picoCTF{secret_keys_to_the_kingdom_45e7608d}'`

> *Note 1: The final 8 letters (in my case "45e7608d") may possibly be different if you try to solve it. Be sure to follow the writeup to get your correct flag.*
> *Note 2: SECRET_KEY is something we'll be using in Flaskcards 2 (AKA "Skeleton Key"), for now you can simply use the `picoCTF{...}` flag*
